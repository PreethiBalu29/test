package com.galen;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GalenSample {

	public class SampleUILayoutTest {
	   
		String googleTestPage = "src\\test\\resources\\specs\\google.gspec";
	    WebDriver driver;
	    LayoutReport layoutReport;
	    public static final String USERNAME = "preethibalasubra_tkyQj3";
		public static final String AUTOMATE_KEY = "3JZ3sYoVcwxGVpmcqyyB";
		public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
		
	    
		
	  
	    @BeforeClass
	    public void init() throws MalformedURLException {
//	    	DesiredCapabilities caps = new DesiredCapabilities();
//			caps.setCapability("os", "Windows");
//			caps.setCapability("os_version", "10");
//			caps.setCapability("browser_version", "latest");
//			caps.setCapability("name", "Browser Stack");
//	    	WebDriverManager.chromedriver().setup();
//		    driver = new ChromeDriver();
			
	    	
	    	DesiredCapabilities caps = new DesiredCapabilities();
			caps.setCapability("os_version", "9.0");
			caps.setCapability("device", "Samsung Galaxy S10e");
			caps.setCapability("real_mobile", "true");
			caps.setCapability("browserstack.local", "false");
			caps.setCapability("name", "GalenMobileTesting");
			caps.setCapability("browser", "Chrome");
			WebDriverManager.chromedriver().setup();
			
			try {
				driver= new RemoteWebDriver(new URL(URL), caps);
				driver.get("https://www.amazon.in/");
				//WebElement search = driver.findElement(By.name("q"));
				//search.sendKeys("Browser Stack");
				
			}
			catch (MalformedURLException e)
			{
				e.printStackTrace();
			}
	    }
	    @Test
	    public void checkLogoVisible() throws IOException {
	        layoutReport = Galen.checkLayout(driver, googleTestPage, Arrays.asList("mobile"));
	    }
	    @AfterMethod
	    public void reportUpdate() {
	        try {
	            List<GalenTestInfo> tests = new LinkedList<>();
	            GalenTestInfo test = GalenTestInfo.fromString("Test Automation Using Galen Framework");
	            test.getReport().layout(layoutReport, "Verify logo");
	            tests.add(test);
	            new HtmlReportBuilder().build(tests, "target/galen-html-reports");
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	 
	    @AfterClass
	    public void tearDown() {
	        driver.quit();
	    }
	 
	}
}
